<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'homestead');

/** MySQL database password */
define('DB_PASSWORD', 'secret');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'VZ*{x_a-s$3:Xl=t(g-$!+>|y2BW6H{z<:2IdcQ)x:{)`A43&XZ9^|0Z+a~a6O M');
define('SECURE_AUTH_KEY',  'Cb0$.^3G9oWUbDD445?Pe)y15CV-|MkoRp_aO48X7JxX/hbBGT9mIz%BTbdy)ltF');
define('LOGGED_IN_KEY',    ')=8~(rnehRZxw`IY6i[c4~;h~!3s|f#DzGFIqon$>^xREqI7_/om=MT;D,).O|dK');
define('NONCE_KEY',        '?^o;/&D^Dv~SwKl)l1im00b6J$@=&[-`$/qs0=lc? `EJVW_@i|f}spP3RvTkikc');
define('AUTH_SALT',        'WiLb]72?p(J Ttx^O9W)4Q{2TE:ZH[+~l384dkT>TE3])T<>Kw|!l<P_GWzXE$!a');
define('SECURE_AUTH_SALT', 'Ft_(`7K_LdNK(/*m-T`ip=fOc-#OE:y@fsR;[Ko+a= k(}db@?w#8FIwJ[C--vyL');
define('LOGGED_IN_SALT',   'R$~J|LnuF@{c3i.,bz9}IqS;_R<1=*Uc|R`~AU<a<+}dijM?}C9B)&YM-7FvR[ #');
define('NONCE_SALT',       'oVa}|tO+C;.Jmj*ah^16Dr|aPV-$DgDY]hA. ?(m{|G/>.>a*O@c{EZ6L7,OXxQJ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
