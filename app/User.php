<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * App\User
 *
 * @mixin \Eloquent
 * @property integer $ID
 * @property string $user_login
 * @property string $user_pass
 * @property string $user_nicename
 * @property string $user_email
 * @property string $user_url
 * @property string $user_registered
 * @property string $user_activation_key
 * @property integer $user_status
 * @property string $display_name
 * @method static \Illuminate\Database\Query\Builder|\App\User whereID($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereUserLogin($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereUserPass($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereUserNicename($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereUserEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereUserUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereUserRegistered($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereUserActivationKey($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereUserStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereDisplayName($value)
 */
class User extends Authenticatable
{
    protected $table = 'wp_users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_login', 'user_email', 'user_pass',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_pass'
    ];
}
