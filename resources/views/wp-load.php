<?php
$wp_load_location_array = array();
$wp_load_location_array[] = "../../../../../../wp-load.php";
$wp_load_location_array[] = "../../../../../wp-load.php";
$wp_load_location_array[] = "../../../../wp-load.php";
$wp_load_location_array[] = "../../../wp-load.php";
$wp_load_location_array[] = "../../wp-load.php";

foreach($wp_load_location_array as $wp_load_location)
{
	if(file_exists($wp_load_location))
	{
		require_once($wp_load_location);
	}
}
?>